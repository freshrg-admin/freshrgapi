const express = require('express')
const app = express()
const BodyParser = require('body-parser')
// export dependencies 

app.use(express.json()) // for json object
app.use(BodyParser.json()) // for read json object
app.use(express.urlencoded({ extended: true })) // for form data

//watcher for get request
app.get('*', (req,res)=>{
  console.log('get api')
  res.sendStatus(200)
})
//watcher for post request
app.post('/api', (req,res)=>{
  console.log('post api')
  console.log(req.body)
  res.sendStatus(200)
})
//start server on port 3000
app.listen(3000,()=> console.log('Server has been started ...'))
